from io import BytesIO
from typing import Optional
from PIL import Image, ImageDraw, ImageFont


def generate_placeholder(w: int, h: Optional[int] = None, border_color: Optional[str] = 'dimgray', fill_color: Optional[str] = 'gray'):
    if not h:
        h = w

    img = Image.new('RGB', (w, h), border_color)
    draw = ImageDraw.Draw(img)

    txt = str(w) + '×' + str(h)
    font_size = 1
    font = ImageFont.truetype('font/BebasNeue-Regular.otf', font_size)

    border_width = (h * 0.05) if w > h else (w * 0.05)

    while font.getlength(txt) < img.size[0] - (border_width * 2 + img.size[0] * 0.1) and font.getbbox(txt, anchor='lt')[3] < img.size[1] - (border_width * 2 + img.size[1] * 0.1):
        font_size += 1
        font = ImageFont.truetype('font/BebasNeue-Regular.otf', font_size)

    draw.rectangle((border_width, border_width, w - border_width, h - border_width), fill=fill_color)
    text_height = font.getbbox(txt, anchor='lt')[3]
    v_center = (h / 2) - (text_height / 2)
    draw.text((w / 2,  v_center), txt, anchor="mt", fill='white', font=font)

    buffer = BytesIO()
    img.save(buffer, format='png')

    return buffer.getvalue()
