function updateImage() {
    let img = document.getElementById('image');
    let link = document.getElementById('link');

    let widthInput = document.getElementById('width');
    let width = parseInt(widthInput.value);
    let heightInput = document.getElementById('height');
    let height = parseInt(heightInput.value);
    let borderColorInput = document.getElementById('border');
    let borderColor = borderColorInput.value;
    let fillColorInput = document.getElementById('fill');
    let fillColor = fillColorInput.value;

    if(isNaN(width)) {
        widthInput.classList.add('is-invalid');
    } else {
        widthInput.classList.remove('is-invalid');
    }

    if(isNaN(height)) {
        heightInput.classList.add('is-invalid');
    } else {
        heightInput.classList.remove('is-invalid');
    }

    const regex= /^(?:[0-9a-fA-F]{3}){1,2}$/;

    if(borderColor || fillColor) {
        if(!borderColor.match(regex)) {
            borderColorInput.classList.add('is-invalid');
        } else {
            borderColorInput.classList.remove('is-invalid');
        }

        if(!fillColor.match(regex)) {
            fillColorInput.classList.add('is-invalid');
        } else {
            fillColorInput.classList.remove('is-invalid');
        }
    }

    if((width > 0 && width <= 4000) && (height > 0 && height <= 4000)) {
        if(borderColor.match(regex) && fillColor.match(regex)) {
            let url = 'https://lorempic.com/' + width + '/' + height + '/' + borderColor + '/' + fillColor;
            img.src = url;
            link.innerText = url;
        } else {
            let url = 'https://lorempic.com/' + width + '/' + height;
            img.src = url;
            link.innerText = url;
        }
    }
}

function copyToClipboard() {
    let link = document.getElementById('link').innerText;
    let clip = document.getElementById('clip');

    navigator.clipboard.writeText(link).then(
        () => {
            clip.setAttribute('href', '/static/svg/icons.svg#icon-check');

            setTimeout(() => {
                clip.setAttribute('href', '/static/svg/icons.svg#icon-clipboard');
            }, 1500)
        },
        () => {
            clip.setAttribute('href', '/static/svg/icons.svg#icon-x');

            setTimeout(() => {
                clip.setAttribute('href', '/static/svg/icons.svg#icon-clipboard');
            }, 1500)
        }
    );

}
