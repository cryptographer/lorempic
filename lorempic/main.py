import re

from fastapi import FastAPI, Request, HTTPException
from fastapi.responses import Response, HTMLResponse, FileResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from placeholder import generate_placeholder

app = FastAPI()
app.mount('/static', StaticFiles(directory='static'), name='static')

templates = Jinja2Templates(directory='templates')


@app.get('/', response_class=HTMLResponse)
async def root(request: Request):
    return templates.TemplateResponse('index.htm', {"request": request})


@app.get('/favicon.ico', include_in_schema=False)
async def favicon():
    return FileResponse('favicon.ico')


@app.get('/{w}',
         response_class=Response,
         responses={
             200: {
                 "content": {"image/png": {}}
             }
         })
async def get_pic(w: int):
    if w > 4000:
        raise HTTPException(status_code=400, detail='Requested image exceeds size limits')
    return Response(
        content=generate_placeholder(w),
        media_type='image/png'
    )


@app.get('/{w}/{h}',
         response_class=Response,
         responses={
             200: {
                 "content": {"image/png": {}}
             }
         })
async def get_pic(w: int, h: int):
    if w > 4000 or h > 4000:
        raise HTTPException(status_code=400, detail='Requested image exceeds size limits')
    return Response(
        content=generate_placeholder(w, h),
        media_type='image/png'
    )


@app.get('/{w}/{border}/{fill}',
         response_class=Response,
         responses={
             200: {
                 "content": {"image/png": {}}
             }
         })
async def get_pic(w: int, border: str, fill: str):
    if w > 4000:
        raise HTTPException(status_code=400, detail='Requested image exceeds size limits')
    if re.fullmatch('^(?:[0-9a-fA-F]{3}){1,2}$', border) is None or re.fullmatch('^(?:[0-9a-fA-F]{3}){1,2}$', fill) is None:
        raise HTTPException(status_code=400, detail='Not a valid hex color code')
    return Response(
        content=generate_placeholder(w, border_color='#' + border, fill_color='#' + fill),
        media_type='image/png'
    )


@app.get('/{w}/{h}/{border}/{fill}',
         response_class=Response,
         responses={
             200: {
                 "content": {"image/png": {}}
             }
         })
async def get_pic(w: int, h: int, border: str, fill: str):
    if w > 4000 or h > 4000:
        raise HTTPException(status_code=400, detail='Requested image exceeds size limits')
    if re.fullmatch('^(?:[0-9a-fA-F]{3}){1,2}$', border) is None or re.fullmatch('^(?:[0-9a-fA-F]{3}){1,2}$', fill) is None:
        raise HTTPException(status_code=400, detail='Not a valid hex color code')
    return Response(
        content=generate_placeholder(w, h, '#' + border, '#' + fill),
        media_type='image/png'
    )
