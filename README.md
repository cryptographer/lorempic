# LoremPic

## About

LoremPic is a Python project that uses [FastAPI](https://fastapi.tiangolo.com/) and [Pillow](https://python-pillow.org/) to generate placeholder images. The site uses [Bootstrap](https://getbootstrap.com/) and [Feather](https://feathericons.com/) icons.

## Usage

You can request an image by providing an equal width and height as one dimension:

`<img src="https://lorempic.com/size">`

`<img src="https://lorempic.com/400">`

<img class="mb-3" src="https://lorempic.com/400" alt="Generated image.">

Or provide the width and height separately:

`<img src="https://lorempic.com/width/height">`

`<img src="https://lorempic.com/400/200">`

<img class="mb-3" src="https://lorempic.com/400/200" alt="Generated image.">

You can also specify the border and fill colors as three or six digit hex color codes:

`<img src="https://lorempic.com/size/border/fill">`

`<img src="https://lorempic.com/400/abc/123456">`

<img class="mb-3" src="https://lorempic.com/400/abc/123456" alt="Generated image.">

`<img src="https://lorempic.com/width/height/border/fill">`

`<img src="https://lorempic.com/400/200/123456/abc">`

<img src="https://lorempic.com/400/200/123456/abc" alt="Generated image.">

## License
The code for this project is available under the terms of the <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">AGPLv3</a>.
